$age = Read-Host "Enter your age"

if($age -lt 18) {
	$type = "child"
}
elseif($age -gt 60) {
	$type = "old person"
}
else {
	$type = "normal adult"
}

Write-Output "You are a $type"