param(
	[int]$max = 0
)

$sum = 0

for($i = 0; $i -lt $max; $i = $i + 1) {
	$sum = $sum + $i
}

#Alternative
#for($i = 0; $i -lt $max; $i++) {
#	$sum += $i
#}

#Alternative
# $series = 1..($max - 1)
# foreach($i in $series) {
# 	$sum += $i
# }

Write-Output $sum